import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent} from './components/site/home/home.component';
import {NearmeComponent} from './components/site/nearme/nearme.component';
import {SearchComponent} from './components/site/search/search.component';
import {ManagerDashComponent} from './components/site/manager-dash/manager-dash.component';
import { ParkInfoComponent } from './components/site/park-info/park-info.component';
import {ContactsComponent} from './components/site/contacts/contacts.component';
import {LoginComponent} from './components/site/login/login.component';
import {RegisterComponent} from './components/site/register/register.component';
import {EditParkComponent} from './components/site/edit-park/edit-park.component';
import {ShowSuggestionsComponent} from './components/site/show-suggestions/show-suggestions.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {path: 'home', component: HomeComponent},
  {path: 'nearme', component: NearmeComponent},
  {path: 'search', component: SearchComponent},
  {path: 'manager-dashboard', component: ManagerDashComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'park/:id', component: ParkInfoComponent},
  {path: 'manager-dashboard/edit/:id', component: EditParkComponent},
  {path: 'manager-dashboard/show-suggestions', component: ShowSuggestionsComponent},
  {path: 'contacts', component: ContactsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
