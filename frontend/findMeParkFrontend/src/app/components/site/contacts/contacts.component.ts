import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  constructor() {
  }

  ngOnInit() {
  }

  onSubmit(event: any) {
    const allInfo = `Email send to ${event.target.email.value}`;
    alert(allInfo);
  }
}
