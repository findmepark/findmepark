import { Component, OnInit } from '@angular/core';
import {Parque} from '../../../Models/parque';
import {ApiParquesService} from '../../../Services/api-parques.service';

import {
  ReactiveFormsModule,
  FormsModule,
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit {

  parques: Parque[];
  parksEffective: any[] = new Array();
  renderCards: boolean;
  selectedfilters: string[] = new Array();
  filteredFree: boolean;
  isFree = false;

  constructor(private apiParquesService: ApiParquesService) { }

  ngOnInit() {
    this.filteredFree = false;
  }

  submitSearchForm(searchData) {
    this.apiParquesService.find(searchData.location).subscribe(data => {
      this.parques = data;
      this.parksEffective = data;
      console.log(this.parques);
    });
    console.log(searchData.location);
    this.renderCards = true;
    console.log(this.parksEffective);
  }

  filterFree() {
    if (!this.filteredFree) {
      this.selectedfilters.push('Free');
      this.parques = this.parques.filter(p => p.preco === 0);
      this.filteredFree = true;
      this.isFree = true;
    } else {
      this.selectedfilters.pop();
      this.parques = this.parksEffective;
      this.filteredFree = false;
      this.isFree = false;
    }
  }


}
