import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewParkComponent } from './new-park.component';

describe('NewParkComponent', () => {
  let component: NewParkComponent;
  let fixture: ComponentFixture<NewParkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewParkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewParkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
