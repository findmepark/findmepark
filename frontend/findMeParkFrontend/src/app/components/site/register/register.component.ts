import { Component, OnInit } from '@angular/core';
import { UtilizadorService } from '../../../Services/utilizador.service';
import {Utilizador} from '../../../Models/utilizador';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from '../../../custom-validators';
import {Router} from '@angular/router';
import {HttpResponse} from '@angular/common/http';

// import * as CryptoJS from 'crypto-js';

export class AddUser {
    nome: string;
    email: string;
    telefone: string;
    password: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  private encryptSecretKey: CryptoKey;
  public frmSignup: FormGroup;
  private user : AddUser = new AddUser();
  
  constructor(private fb: FormBuilder, private utilizadorService: UtilizadorService, private router: Router) {
    this.frmSignup = this.createSignupForm();
  }

  ngOnInit() {
  }

  createSignupForm(): FormGroup {
    return this.fb.group(
        {
          nome: [null, Validators.compose([
              Validators.minLength(3),
              ])
          ],
          // email is required and must be a valid email email
          email: [null, Validators.compose([
            Validators.email,
            Validators.required])
          ],
          telefone: [null, Validators.compose([Validators.minLength(9)])],
          password: [ null, Validators.compose([
            // 1. Password Field is Required
            Validators.required,
            CustomValidators.patternValidator(/\d/, { hasNumber: true }),
            CustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
            CustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
            Validators.minLength(8)])
          ],
          repeatpass: [null, Validators.compose([Validators.required])]
        },
        {
          // check whether our password and confirm password match
          validator: CustomValidators.passwordMatchValidator
        });
  }

  submit() {
      // this.frmSignup.value.password = this.encryptData(this.frmSignup.value.password);
      // console.log(this.encryptData(this.frmSignup.value.password));
      // console.log(this.frmSignup.value);
      this.user.nome = this.frmSignup.get('nome').value;
      this.user.email = this.frmSignup.get('email').value;
      this.user.telefone = this.frmSignup.get('telefone').value;
      this.user.password = this.frmSignup.get('password').value;
      this.add_user();
  }

  add_user() {
    this.utilizadorService.addUtilizador(this.user)
      .subscribe(data => {
        alert('Your account was registered with success!');
        this.router.navigateByUrl('login');
      }, (error: HttpResponse<any>) => {
        console.log(error);
      });


    // this.utilizadorservice.addutilizador(this.data)
    //   .subscribe(data => {
    //     alert('your account was registered with success!');
    //     this.router.navigatebyurl('login');
    //   }, (error: httpresponse<any>) => {
    //     console.log(error);
    //   });

  }

  // encryptData(data): string {
  //     console.log(data);
  //     try {
  //         return CryptoJS.AES.encryptData(data.toString()).toString();
  //     } catch (e) {
  //         console.log(e);
  //     }
  // }
}
