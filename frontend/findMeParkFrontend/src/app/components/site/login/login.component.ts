import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { UtilizadorService } from 'src/app/Services/utilizador.service';
import {Router} from '@angular/router';
import {HttpResponse} from '@angular/common/http';

export class LogInUser {
    email: string;
    password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  validatingForm: FormGroup;
  private user: LogInUser = new LogInUser();
  
  constructor(private utilizadorService: UtilizadorService, private router: Router) {
  }

  ngOnInit() {
    this.validatingForm = new FormGroup({
      modalFormDarkEmail: new FormControl('', Validators.email),
      modalFormDarkPassword: new FormControl('', Validators.required)
    });
  }

  submit() {
    console.log(this.validatingForm);
    this.user.email = this.validatingForm.get('modalFormDarkEmail').value;
    this.user.password = this.validatingForm.get('modalFormDarkPassword').value;
    this.login_user();
  }

  login_user() {
    this.utilizadorService.logInUtilizador(this.user)
      .subscribe(data => {
        alert('Log in success!');
        localStorage.setItem('logged_in', this.user.email);
        this.router.navigateByUrl('manager-dashboard');
      }, (error: HttpResponse<any>) => {
        console.log(error);
      });
  }

  get modalFormDarkEmail() {
    return this.validatingForm.get('modalFormDarkEmail');
  }

  get modalFormDarkPassword() {
    return this.validatingForm.get('modalFormDarkPassword');
  }
}
