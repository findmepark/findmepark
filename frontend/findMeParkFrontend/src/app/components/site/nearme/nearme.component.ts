import { Component, OnInit } from '@angular/core';
import {Parque} from '../../../Models/parque';
import {ApiParquesService} from '../../../Services/api-parques.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-nearme',
  templateUrl: './nearme.component.html',
  styleUrls: ['./nearme.component.css']
})
export class NearmeComponent implements OnInit {
  title = 'Map';
  latitude: number;
  longitude: number;
  zoom: number;
  parques: Parque[];
  private icon: { scaledSize: { width: number; height: number }; url: any };


  constructor(private apiParquesService: ApiParquesService, private route: ActivatedRoute ) { }

  ngOnInit() {
    this.setCurrentLocation();
    this.icon = {
      url: require('src/assets/images/pin.png'),
      scaledSize: {
        height: 43,
        width: 40,
      }
    };
  }

  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
        this.apiParquesService.findNearMe(this.latitude, this.longitude).subscribe(data => {
          this.parques = data;
          console.log(this.parques);
        });
      });
    }
  }
}


