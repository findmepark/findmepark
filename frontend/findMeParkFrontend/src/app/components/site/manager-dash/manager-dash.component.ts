import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import {ApiSensoresService} from '../../../Services/api-sensores.service';
import {SensorParque} from '../../../Models/SensorParque';
import {UtilizadorService} from '../../../Services/utilizador.service';
import {Utilizador} from '../../../Models/utilizador';

export interface PeriodicElement {
  type: string;
  position: number;
  plate: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, type: 'IN', plate: '82-CK-56'},
  {position: 2, type: 'OUT', plate: '82-CK-57'},
  {position: 3, type: 'IN', plate: '82-CK-55'},
  {position: 4, type: 'IN', plate: '83-CK-56'},
  {position: 5, type: 'OUT', plate: '82-CK-56'},
  {position: 6, type: 'OUT', plate: '82-CK-56'},
  {position: 7, type: 'OUT', plate: '82-CK-56'},
  {position: 8, type: 'OUT', plate: '82-CK-56'},
  {position: 9, type: 'OUT', plate: '82-CK-56'},
  {position: 10, type: 'OUT', plate: '82-CK-56'},
];

@Component({
  selector: 'app-manager-dash',
  templateUrl: './manager-dash.component.html',
  styleUrls: ['./manager-dash.component.css']
})
export class ManagerDashComponent implements OnInit {

  private user: Utilizador;
  private userEmail: string;
  constructor(private sensoresService: ApiSensoresService, private utilizadorService: UtilizadorService ) {
    this.userEmail = localStorage.getItem('logged_in');
  }

  sideBarOpen = false;
  sideBarClosed = !this.sideBarOpen;

  sensores: SensorParque[];

  displayedColumns: string[] = ['position', 'type', 'plate'];
  dataSource = ELEMENT_DATA;

  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
  };
  public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;

  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];
  chartOptions: any;

  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public randomize(): void {
    this.barChartType = this.barChartType === 'bar' ? 'line' : 'bar';
  }

  getSensorsParque(): void {
    this.sensoresService.findAll().subscribe(sensores => this.sensores = sensores);
  }

  ngOnInit() {
    this.getSensorsParque();
    console.log(this.sensores);
    this.getUser();
  }


  getUser(): void {
    this.utilizadorService.searchbyemail(this.userEmail).subscribe(user => {
      this.user = user;
    });
  }

  toggleSideBar() {
    this.sideBarOpen = !this.sideBarOpen;
  }

  logout() {
    localStorage.setItem('logged_in', '');
  }
}
