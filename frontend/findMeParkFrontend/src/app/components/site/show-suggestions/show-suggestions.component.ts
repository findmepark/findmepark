import { Component, OnInit } from '@angular/core';
import {UtilizadorService} from '../../../Services/utilizador.service';
import {Utilizador} from '../../../Models/utilizador';

@Component({
  selector: 'app-show-suggestions',
  templateUrl: './show-suggestions.component.html',
  styleUrls: ['./show-suggestions.component.css']
})
export class ShowSuggestionsComponent implements OnInit {
  private user: Utilizador;
  private userEmail: string;
  private suggestions: any;
  constructor(private utilizadorService: UtilizadorService) {
    this.userEmail = localStorage.getItem('logged_in');
  }

  ngOnInit() {
    this.getUser();
  }

  getUser(): void {
    this.utilizadorService.searchbyemail(this.userEmail).subscribe(user => {
      this.user = user;
      this.suggestions = user.sugestao;
      console.log(this.suggestions);
    });
  }

}
