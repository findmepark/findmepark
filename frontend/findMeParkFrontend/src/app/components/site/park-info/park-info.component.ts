import { Component, OnInit } from '@angular/core';
import {Parque} from '../../../Models/parque';
import {ApiParquesService} from '../../../Services/api-parques.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-park-info',
  templateUrl: './park-info.component.html',
  styleUrls: ['./park-info.component.css']
})

export class ParkInfoComponent implements OnInit {

    parque_id: number;
    parque: Parque;
    available: boolean;

  parksEffective: any[] = new Array();
  parks: any[] = new Array();

  renderCards: boolean;
  selectedfilters: string[] = new Array();

  constructor(private apiParquesService: ApiParquesService, private route: ActivatedRoute ) { }

  ngOnInit() {
    this.parque_id = +this.route.snapshot.paramMap.get('id');
    console.log(this.parque_id);
    this.apiParquesService.find_by_id(this.parque_id).subscribe(data => {
      this.parque = data;
      this.available = data.disponivel;
      console.log(this.parque);

      if (this.parque.nome == null) {
        this.parque.nome = 'Parque';
      }
      if (this.parque.localizacao == null) {
        this.parque.localizacao = 'Cidade';
      }
    });
  }
}
