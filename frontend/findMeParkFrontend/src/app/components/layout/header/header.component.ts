import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  logged: any;
  islogged : boolean;

  constructor() { }

  ngOnInit() {
    this.logged = localStorage.getItem('logged_in');
    if (this.logged !== '') {
      this.islogged = true;
    } else {
      this.islogged = false;
    }
  }

}
