import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { HomeComponent } from './components/site/home/home.component';
import { NearmeComponent } from './components/site/nearme/nearme.component';
import { HttpClientModule } from '@angular/common/http';
import { SearchComponent } from './components/site/search/search.component';
import { ApiParquesService } from './Services/api-parques.service';
import { FirstPageComponent } from './components/site/home/first-page/first-page.component';
import { ManagerDashComponent } from './components/site/manager-dash/manager-dash.component';
import { AgmCoreModule } from '@agm/core';
import { ParkInfoComponent } from './components/site/park-info/park-info.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';
import { ChartsModule } from 'ng2-charts';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HighchartsChartModule} from 'highcharts-angular';
import { ContactsComponent } from './components/site/contacts/contacts.component';
import { RegisterComponent } from './components/site/register/register.component';
import { LoginComponent } from './components/site/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
/* Angular material */
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {MatMenuModule} from '@angular/material';
import { NewParkComponent } from './components/site/new-park/new-park.component';
import { EditParkComponent } from './components/site/edit-park/edit-park.component';
import { ShowSuggestionsComponent } from './components/site/show-suggestions/show-suggestions.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    NearmeComponent,
    SearchComponent,
    FirstPageComponent,
    ManagerDashComponent,
    ParkInfoComponent,
    ContactsComponent,
    RegisterComponent,
    LoginComponent,
    NewParkComponent,
    EditParkComponent,
    ShowSuggestionsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    ChartsModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    AgmCoreModule.forRoot({

      apiKey: 'AIzaSyC15C5afG_rqZOMlctBd9S2ZMorUnTzLC8'
    }),
    HighchartsChartModule,
    MatMenuModule
  ],
  providers: [ApiParquesService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
