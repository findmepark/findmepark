export class Parque {
  id: number;
  nome: string;
  ocupacao: number;
  disponivel: boolean;
  localizacao: string;
  preco: number;
  totalLugares: number;
  latitude: number;
  longitude: number;
  localizacaoParque: any;
}
