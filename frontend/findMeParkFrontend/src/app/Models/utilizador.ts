export class Utilizador {
    id: number;
    nome: string;
    email: string;
    telefone: string;
    password: string;
    sugestao: any;
}
