import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Utilizador} from '../Models/utilizador';
import { catchError, retry } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({ providedIn: 'root' })
export class UtilizadorService {
  private utilizadoresUrl: string;


  constructor(private http: HttpClient) {
    this.utilizadoresUrl = 'http://localhost:8080/Utilizadores/';
  }

  // addUtilizador(nome: string, email : string, telefone : string, password : string) : any{
  addUtilizador(user : any) : Observable<any>{
    // let o = {nome, email, telefone, password};
    console.log(JSON.parse(JSON.stringify(user)));
    let ret = this.http.post(this.utilizadoresUrl + 'create/', JSON.parse(JSON.stringify(user)), httpOptions);
    console.log(ret);
    return ret;
  }

  logInUtilizador(user: any): Observable<any> {
    return this.http.post(this.utilizadoresUrl + 'login/', JSON.parse(JSON.stringify(user)), httpOptions);
  }

  searchbyemail(email: any): Observable<any> {
    return this.http.get(this.utilizadoresUrl + 'searchbyemail/' + email);
  }

}
