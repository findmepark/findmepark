import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {SensorParque} from '../Models/SensorParque';

@Injectable({
  providedIn: 'root'
})
export class ApiSensoresService {

  private sensorsUrl: string;

  constructor(private http: HttpClient) {
    this.sensorsUrl = 'http://localhost:8080/SensorPresenca/findall';
  }

  public findAll(): Observable<SensorParque[]> {
    console.log(this.http.get<SensorParque[]>(this.sensorsUrl));
    return this.http.get<SensorParque[]>(this.sensorsUrl);
  }
}
