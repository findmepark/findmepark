import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Parque} from '../Models/parque';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiParquesService {

  private parquesUrl: string;

  constructor(private http: HttpClient) {
    this.parquesUrl = 'http://localhost:8080/Parques/';
  }

  public findAll(): Observable<Parque[]> {
    let ret = this.http.get<Parque[]>(this.parquesUrl + 'findall/');
    return ret;
  }

  public find_by_id(id: number): Observable<Parque> {
    console.log(this.http.get<Parque>(this.parquesUrl + 'searchbyid/' + id));
    return this.http.get<Parque>(this.parquesUrl + 'searchbyid/' + id);
  }

  public find(localizacao: string): Observable<Parque[]> {
    console.log(this.http.get<Parque[]>(this.parquesUrl + 'searchbylocalizacao/' + localizacao));
    return this.http.get<Parque[]>(this.parquesUrl + 'searchbylocalizacao/' + localizacao);
  }

  public findNearMe(latitude : number, longitude: number): Observable<Parque[]>{
    // console.log(latitude + " " + longitude);
    let ret = this.http.get<Parque[]>(this.parquesUrl + 'searchbycoordenadas/' + latitude +"/" + longitude);
    // return this.http.get<Parque[]>(this.parquesUrl + latitude+"/" + longitude);
    // console.log(ret);
    return ret;
  }

}



