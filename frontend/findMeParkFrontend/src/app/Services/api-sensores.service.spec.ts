import { TestBed } from '@angular/core/testing';

import { ApiSensoresService } from './api-sensores.service';

describe('ApiSensoresService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiSensoresService = TestBed.get(ApiSensoresService);
    expect(service).toBeTruthy();
  });
});
