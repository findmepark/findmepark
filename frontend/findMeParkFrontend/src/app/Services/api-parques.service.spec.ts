import { TestBed } from '@angular/core/testing';

import { ApiParquesService } from './api-parques.service';

describe('ApiParquesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiParquesService = TestBed.get(ApiParquesService);
    expect(service).toBeTruthy();
  });
});
