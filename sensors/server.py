import json
import selectors
import socket
import threading
import redis

HOST = "localhost"
PORT = 1024

# # RabbitMQ
# connection = pika.BlockingConnection(
#     pika.ConnectionParameters(host='rabbit')
# )   
# channel = connection.channel()
# channel.queue_declare(queue = 'sensors_data_stream')

r = redis.Redis(host = 'redis')

sel = selectors.DefaultSelector()

# def sendRabbitMQ(channel, msg):
#     channel.basic_publish(exchange='', routing_key='sensors_data_stream', body=msg)
#     print("sent msg to rabbitmq")

def publish_to_redis(value):
    e_code = r.publish('message_broker', json.dumps(value).encode('utf-8'))
    # print('published to redis (', e_code, '):', value)

def insert_to_redis(key, value):
    r.set(key, json.dumps(value))
    # print('set to redis')

def accept(sock, mask):
    global client_list

    (conn, addr) = sock.accept() # returns new socket and addr.
    if conn not in client_list:
        client_list.append(conn)

    print('accepted', conn, 'from', addr);
    msg = {'type' : 'OK'}
    conn.send(json.dumps(msg).encode('utf-8'))

    conn.setblocking(False) # program doesn't wait
    sel.register(conn, selectors.EVENT_READ, read)

def read(conn, mask):
    global client_list
    data = conn.recv(1024)  # Should be ready
    if data:
        decoded_data = json.loads(data.decode('utf-8'))
        # print()
        # print('received', str(decoded_data))

        # sendRabbitMQ(channel, data)

        if decoded_data.get('type') == 'SENSOR_CREATE':
            # publish_to_redis(decoded_data.get('id')+':name', decoded_data.get('name'))
            publish_to_redis(decoded_data)
        elif decoded_data.get('type') == 'SENSOR_DATA':
            # insert_to_redis(decoded_data.get('id'), decoded_data.get('data'))
            publish_to_redis(decoded_data)
    else:
        print('closing', conn)
        sel.unregister(conn)
        # channel.close()
        conn.close()

sock = socket.socket() # creates new socket
sock.bind((HOST, PORT)) # binds socket to localhost on port 1024
sock.listen(100) # socket will accept up to 100 connections
sock.setblocking(False) # socket is non-blocking
sel.register(sock, selectors.EVENT_READ, accept) # selector registers socket for selection, for EVENT_READ, and callback is accept()

client_list = []

while True:
    events = sel.select() # blocks in select()
    for key, mask in events:
        callback = key.data
        callback(key.fileobj, mask)




