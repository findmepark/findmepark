import socket
import time
import json

from sensors import *

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 1024        # The port used by the server

def main(args):
    
    SENSOR_ID = args.id
    SENSOR_NAME = args.name

    if args.sensor_type == 'parking_space':
        sensor = ParkingSpaceSensor()
    elif args.sensor_type == 'camera':
        sensor = CameraSensor()

    print('Sensor:', sensor)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))

        data = json.loads(s.recv(1024).decode('utf-8'))

        if not 'type' in data or data.get('type') != 'OK':
            return

        msg = {
                'type' : 'SENSOR_CREATE',
                'id' : f'{SENSOR_ID}',
                'name' : f'{SENSOR_NAME}',
                'sensor_type' : f'{args.sensor_type}'
            }
        msg_b = json.dumps(msg).encode('utf-8')

        # print('Sending', repr(msg_b))
        s.send(msg_b)

        while True:
            time.sleep(2)

            msg = {
                    'type' : 'SENSOR_DATA',
                    'id' : f'{SENSOR_ID}',
                    'name' : f'{SENSOR_NAME}',
                    'data' : sensor.random_sensor_value()
                }
            msg_b = json.dumps(msg).encode('utf-8')

            # print('Sending', repr(msg_b))
            s.send(msg_b)
            # data = s.recv(1024)
            # if not data:
            #     continue

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Sensor')
    parser.add_argument("--id", default=1, type=int, help="Sensor ID")
    parser.add_argument("--name", default="Sensor", type=str, help="Sensor name")
    parser.add_argument("--sensor-type", default="parking_space", type=str, help="Sensor type")

    args = parser.parse_args()

    main(args)
