#!/bin/bash

echo "Redis and WebServer are up. Start sensors."

sleep 30

echo "Populating DB..."
curl http://web-server:8080/teste/create
echo "Done"

# Start the server process
python3 server.py &
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start server: $status"
  exit $status
fi

sleep 2;

# Start the client process
for i in {1..4}
do
  python3 client.py --id $i --name "Lugar$i" --sensor-type "parking_space" &
  status=$?
  if [ $status -ne 0 ]; then
    echo "Failed to start client_$i: $status"
    exit $status
  fi
done

# Start the client process for other sensors
for i in {5..8}
do
  python3 client.py --id $i --name "Parque$i" --sensor-type "camera" &
  status=$?
  if [ $status -ne 0 ]; then
    echo "Failed to start client_$i: $status"
    exit $status
  fi
done

# # Start the client process
# python3 client.py --id 2 --name 'Lugar2' &
# status=$?
# if [ $status -ne 0 ]; then
#   echo "Failed to start client_2: $status"
#   exit $status
# fi

# # Start the client process
# python3 client.py --id 3 --name 'Lugar3' &
# status=$?
# if [ $status -ne 0 ]; then
#   echo "Failed to start client_2: $status"
#   exit $status
# fi

# # Start the client process
# python3 client.py --id 4 --name 'Lugar4' &
# status=$?
# if [ $status -ne 0 ]; then
#   echo "Failed to start client_2: $status"
#   exit $status
# fi

# Naive check runs checks once a minute to see if either of the processes exited.
# This illustrates part of the heavy lifting you need to do if you want to run
# more than one service in a container. The container exits with an error
# if it detects that either of the processes has exited.
# Otherwise it loops forever, waking up every 60 seconds

while sleep 60; do
  ps aux |grep server.py |grep -q -v grep
  PROCESS_1_STATUS=$?
  ps aux |grep client.py |grep -q -v grep
  PROCESS_2_STATUS=$?
  # If the greps above find anything, they exit with 0 status
  # If they are not both 0, then something is wrong
  if [ $PROCESS_1_STATUS -ne 0 -o $PROCESS_2_STATUS -ne 0 ]; then
    echo "One of the processes has already exited."
    exit 1
  fi
done
