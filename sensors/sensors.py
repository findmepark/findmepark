
import random
import time

class Sensor():

    def __init__(self):
        self.cicles = 0
        self.cur_val = 0
        self.cicle_limit = random.randint(10, 50) # number of cicles until change

    def random_sensor_value(self):
        self.invert_val()
        return self.cur_val

    def invert_val(self):
        if self.cur_val == 0:
            self.cur_val = 1
        else:
            self.cur_val = 0

class ParkingSpaceSensor(Sensor):

    def random_sensor_value(self):
        self.cicles += 1
        if self.cicles == self.cicle_limit:
            self.cicles = 0
            self.invert_val()
        return self.cur_val

class CameraSensor(Sensor):

    def __init__(self, c=None):
        super().__init__()
        self.plates = []
        if c is None:
            self.plates_limit = self.cicle_limit
        else:
            self.plates_limit = c
        # print('Park size: ' + str(self.plates_limit))

    def random_sensor_value(self):
        if len(self.plates) == self.plates_limit:
            # print('Removing...')
            p = self.remove_plate()
            return {'type' : 'OUT', 'plate' : p}
        else:
            p = self.new_plate()
            self.plates.append(p)
            return {'type' : 'IN', 'plate' : p}

    def remove_plate(self):
        p = random.choice(self.plates)
        self.plates.remove(p)
        return p

    def new_plate(self):
        # generate random number plate (PT)
        numberPlate = ""

        # 1st 2 numbers
        for _ in range(2):
            numberPlate += str(random.randint(0, 9))

        numberPlate += '-'

        # 2nd 2 chars
        for _ in range(2):
            upperCaseLetter = chr(random.randint(65,90)) #Generate a random Uppercase letter (based on ASCII code)
            numberPlate = numberPlate + upperCaseLetter

        numberPlate += '-'

        # 3rd 2 numbers
        for _ in range(2):
            numberPlate += str(random.randint(0, 9))

        return numberPlate

