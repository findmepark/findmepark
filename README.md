# FindMePark

FindMePark repo for IES Project

## Group

86044: Ana Rafaela Vieira
88751: André Gual
88835: Bernardo Rodrigues
89059: João Carvalho
89234: João Marques

## Backlog

https://www.pivotaltracker.com/n/projects/2410457

## Reports

Reports will be available at ./reports/

## Compile/Build

In order to have the mount point for the postgres docker image, we need to create the directory:
  `mkdir /database/postgres/data/`
(This folder is not on git because it is ignored by git)

~~The Spring Boot project can be compiled using `mvn package -Dmaven.test.skip=true`~~

The build is managed completly by docker. Just run the command as explained next.

## Run

After being compiled, the project can be tested using docker: `docker-compose up`

## Deployment

The deployed version in the VM is not fully operational due to problems in communicatin between the components.

When testing on our local machine, the docker container serving the angular website was able to communicate with the API through the localhost URL, and not with the docker URL that usaully works between containers. 

We were expecting this to remain true when deployedin the VM, but it isn't. We think this may be due to a problem in our configuration in relation to the host operating system on the VM and expect to fix it soon.

Meanwhile, the project is fully operational as shown in the presentation in class running in localhost as shown above.

## API Docs

- http://localhost:8080/Utilizadores/findall
- <strong>method:</strong> GET
- <strong>args:</strong> null
- <strong>returns:</strong> lista de utilizadores

<br>

- http://localhost:8080/Utilizadores/create
- <strong>method:</strong> POST
- <strong>args:</strong> utilizador
- <strong>returns:</strong> ok

<br>

- http://localhost:8080/Utilizadores/login
- <strong>method:</strong> POST
- <strong>args:</strong> email, password
- <strong>returns:</strong> ok se coincidir

<br>

- http://localhost:8080/Utilizadores/
- <strong>method:</strong> DELETE
- <strong>args:</strong> utilizador
- <strong>returns:</strong> "Utilizador removido"

<br>

- http://localhost:8080/Localizacao/findall
- <strong>method:</strong> GET
- <strong>args:</strong> null
- <strong>returns:</strong> lista de localizações

<br>

- http://localhost:8080/Localizacao/
- <strong>method:</strong> POST
- <strong>args:</strong> localizacao
- <strong>returns:</strong> "Localização criada"

<br>

- http://localhost:8080/Localizacao/searchbycoordenadas/{latitude}/{longitude}
- <strong>method:</strong> GET
- <strong>args:</strong> latitude, longitude
- <strong>returns:</strong> localizacao

<br>

- http://localhost:8080/Localizacao/searchLocal/{local}
- <strong>method:</strong> GET
- <strong>args:</strong> local(rua)
- <strong>returns:</strong> localizacoes

<br>

- http://localhost:8080/Localizacao/searchByCity/{cidade}
- <strong>method:</strong> GET
- <strong>args:</strong> cidade
- <strong>returns:</strong> localizacoes

<br>

- http://localhost:8080/Localizacao/
- <strong>method:</strong> DELETE
- <strong>args:</strong> localizacao
- <strong>returns:</strong> "localizacao removida"

<br>

- http://localhost:8080/Lugar/findall
- <strong>method:</strong> GET
- <strong>args:</strong> null
- <strong>returns:</strong> lista de lugares

<br>

- http://localhost:8080/Lugar/
- <strong>method:</strong> PUT
- <strong>args:</strong> lugar
- <strong>returns:</strong> "Lugar criada"

<br>

- http://localhost:8080/Lugar/
- <strong>method:</strong> DELETE
- <strong>args:</strong> lugar
- <strong>returns:</strong> "lugar removido"

<br>

- http://localhost:8080/Managers/findall
- <strong>method:</strong> GET
- <strong>args:</strong> null
- <strong>returns:</strong> lista de managers

<br>

- http://localhost:8080/Managers/
- <strong>method:</strong> PUT
- <strong>args:</strong> manager
- <strong>returns:</strong> "Utilizador criado"

<br>

- http://localhost:8080/Managers/delete
- <strong>method:</strong> DELETE
- <strong>args:</strong> manager
- <strong>returns:</strong> "Manager removido"

<br>

- http://localhost:8080/Parques/findall
- <strong>method:</strong> GET
- <strong>args:</strong> null
- <strong>returns:</strong> lista de parques

<br>

- http://localhost:8080/Parques/
- <strong>method:</strong> PUT
- <strong>args:</strong> parque
- <strong>returns:</strong> "Parque criado"

<br>

- http://localhost:8080/Parques/alter
- <strong>method:</strong> POST
- <strong>args:</strong> parque
- <strong>returns:</strong> "Parque alterado"

<br>

- http://localhost:8080/Parques/
- <strong>method:</strong> DELETE
- <strong>args:</strong> parque
- <strong>returns:</strong> "parque removido"

<br>

- http://localhost:8080/SensorPresenca/findall
- <strong>method:</strong> GET
- <strong>args:</strong> null
- <strong>returns:</strong> lista de sensores

<br>

- http://localhost:8080/SensorPresenca/
- <strong>method:</strong> PUT
- <strong>args:</strong> SensorPresenca
- <strong>returns:</strong> "SensorPresenca criado"

<br>

- http://localhost:8080/SensorPresenca/
- <strong>method:</strong> DELETE
- <strong>args:</strong> sensor
- <strong>returns:</strong> "SensorPresença removido"

<br>

- http://localhost:8080/SensorParque/findall
- <strong>method:</strong> GET
- <strong>args:</strong> null
- <strong>returns:</strong> lista de sensores

<br>

- http://localhost:8080/SensorParque/
- <strong>method:</strong> PUT
- <strong>args:</strong> SensorParque
- <strong>returns:</strong> "SensorParque criado"

<br>

- http://localhost:8080/SensorParque/
- <strong>method:</strong> DELETE
- <strong>args:</strong> sensorParque
- <strong>returns:</strong> "SensorParque removido"

<br>

- http://localhost:8080/Sugestao/findall
- <strong>method:</strong> GET
- <strong>args:</strong> null
- <strong>returns:</strong> lista de sugestoes

<br>

- http://localhost:8080/Sugestao/
- <strong>method:</strong> PUT
- <strong>args:</strong> sugestao
- <strong>returns:</strong> "Sugestao criada"

<br>

- http://localhost:8080/Sugestao/
- <strong>method:</strong> DELETE
- <strong>args:</strong> sugestao
- <strong>returns:</strong> "sugestao removida"

<br>

- http://localhost:8080/TempoEstacionamento/findall
- <strong>method:</strong> GET
- <strong>args:</strong> null
- <strong>returns:</strong> lista de Tempos de Estacionamento

<br>

- http://localhost:8080/TempoEstacionamento/
- <strong>method:</strong> PUT
- <strong>args:</strong> TempoEstacionamento
- <strong>returns:</strong> "TempoEstacionamento criado"

<br>

- http://localhost:8080/TempoEstacionamento/
- <strong>method:</strong> DELETE
- <strong>args:</strong> TempoEstacionamento
- <strong>returns:</strong> "TempoEstacionamento removido"

<br>

- http://localhost:8080/Viaturas/findall
- <strong>method:</strong> GET
- <strong>args:</strong> null
- <strong>returns:</strong> lista de Tempos de Estacionamento

<br>

- http://localhost:8080/Viaturas/
- <strong>method:</strong> PUT
- <strong>args:</strong> Viatura
- <strong>returns:</strong> "Viatura criado"

<br>

- http://localhost:8080/Viaturas/delete
- <strong>method:</strong> DELETE
- <strong>args:</strong> Viaturas
- <strong>returns:</strong> "Viaturas removida"