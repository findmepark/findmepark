#!/bin/bash

echo "Pulling most recent changes..."
git pull origin master
echo "Done."

echo "Building docker images..."
docker-compose build
echo "Done."

echo "Deploying in backgroud..."
docker-compose up -d
echo "Done."
