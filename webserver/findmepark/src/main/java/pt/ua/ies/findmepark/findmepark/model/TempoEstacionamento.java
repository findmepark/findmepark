package pt.ua.ies.findmepark.findmepark.model;

import java.util.Date;
import javax.persistence.*;
import java.io.Serializable;
import lombok.Data;

@Data

@Entity
@Table(name = "tempoEstacionamento")
public class TempoEstacionamento implements Serializable {

    private static final long serialVersionUID = -3009157732242241606L;
    @Id
    private long id;

    @Column(name = "data_inicio")
    private Date data_inicio;

    @Column(name = "data_fim")
    private Date data_fim;


    //Relationships
    @ManyToOne
    @JoinColumn
    private Viatura viatura;

    @ManyToOne
    @JoinColumn
    private Parque tempos_parque;

    protected TempoEstacionamento() {
    }

    public TempoEstacionamento(Date data_inicio, Date data_fim) {
        this.data_inicio = data_inicio;
        this.data_fim = data_fim;
    }

    @Override
    public String toString() {
        return String.format("Tempo estacionamento [data_inicio='%s', data_fim='%s']", data_inicio, data_fim);
    }

}
