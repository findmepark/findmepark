package pt.ua.ies.findmepark.findmepark.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pt.ua.ies.findmepark.findmepark.model.*;
import pt.ua.ies.findmepark.findmepark.repository.LocalizacaoRepository;
import pt.ua.ies.findmepark.findmepark.repository.ParqueRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.http.MediaType;


@RestController
@RequestMapping("/Parques")
@CrossOrigin(origins = "*")
public class ParqueController {

    @Autowired
    ParqueRepository repository;
    @Autowired
    LocalizacaoRepository localizacaoRepository;

    @PutMapping("/")
    public String create(@RequestBody Parque parque){
        repository.save(parque);
        return "Parque criado";
    }

    @PostMapping("/alter")
    public String alter(@RequestBody Parque parque){
        repository.save(parque);
        return "Parque alterado";
    }


    @GetMapping("/findall")
    public List<Parque> findAll(){
        List<Parque> Parques = repository.findAll();

        for (Parque p: Parques){
            int ocupacao = p.getLugaresOcupados();
            p.setOcupacao(ocupacao);
            repository.save(p);
        }

        Parques = repository.findAll();

        return Parques;
    }

    @DeleteMapping("/")
    public String delete(@RequestBody Parque parque){
        repository.delete(parque);
        return "Parque removido";
    }

    @RequestMapping("/searchbynome/{nome}")
    public List<Parque> fetchDataByNome(@PathVariable String nome){
        return repository.findByNome(nome);
    }

    @RequestMapping("/searchbyid/{id}")
    public Parque fetchDataById(@PathVariable long id){
        for (Parque p: repository.findAll()){
            int ocupacao = p.getLugaresOcupados();
            p.setOcupacao(ocupacao);
            repository.save(p);
        }
        return repository.findById(id);
    }

    @RequestMapping("/searchbydisponivel/{disponivel}")
    public List<Parque> fetchDataByDisponivel(@PathVariable boolean disponivel){
        for (Parque p: repository.findAll()){
            int ocupacao = p.getLugaresOcupados();
            p.setOcupacao(ocupacao);
            repository.save(p);
        }

        return repository.findByDisponivel(disponivel);
    }

    @RequestMapping(value = "/searchbylocalizacao/{local}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Parque> fetchDataByLocalizacaoCidade(@PathVariable String local) {
        for (Parque p: repository.findAll()){
            int ocupacao = p.getLugaresOcupados();
            p.setOcupacao(ocupacao);
            repository.save(p);
        }

        List<Parque> a = repository.findByLocalizacaoParque_Cidade(local);
        List<Parque> b = repository.findByLocalizacaoParque_Morada(local);

        a.addAll(b);

        return a;

    }

    @RequestMapping("/searchbycoordenadas/{latitude}/{longitude}")
    public List<Parque> fetchDataByLocalizacao(@PathVariable float latitude, @PathVariable float longitude) {
        for (Parque p: repository.findAll()){
            int ocupacao = p.getLugaresOcupados();
            p.setOcupacao(ocupacao);
            repository.save(p);
        }



        List<Localizacao> todos = localizacaoRepository.findAll();
        List<Localizacao> nearme = new ArrayList<>();
        List<Parque> Parques = new ArrayList<>();
        for( Localizacao l : todos){
            float lat = (float) l.getLatitude();
            float longi = (float) l.getLongitude();
            double theta = longi - longitude;
            double dist = Math.sin(Math.toRadians(lat)) * Math.sin(Math.toRadians(latitude)) + Math.cos(Math.toRadians(lat)) * Math.cos(Math.toRadians(latitude)) * Math.cos(Math.toRadians(theta));
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            dist = dist * 1.609344;
            if(dist<=10){
                nearme.add(l);
            }
        }
        for (Localizacao ln: nearme){
            List<Parque> parque2 = repository.findByLocalizacaoParque_Id(ln.getId());
                Parques.addAll(parque2);
        }
        return Parques;
    }

}
