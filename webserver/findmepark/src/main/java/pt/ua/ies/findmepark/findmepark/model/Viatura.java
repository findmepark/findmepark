package pt.ua.ies.findmepark.findmepark.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data

@EqualsAndHashCode()

@Entity
@Table(name = "viatura")
public class Viatura implements Serializable {


    private static final long serialVersionUID = -3009157732242241606L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "matricula")
    private String matricula;


    // Relationship
    // Utilizador many to one
    @ManyToOne
    @JoinColumn
    private Utilizador utilizador;

    // 1 viatura tem varios tempos, 1 tempo tem uma viatura
    @OneToMany(mappedBy = "viatura", cascade = CascadeType.ALL)
    private Set<TempoEstacionamento> tempos;



    protected Viatura() {
    }

    public Viatura(String matricula, TempoEstacionamento... tempos_de_estacionamento) {
        this.matricula = matricula;
        this.tempos = Stream.of(tempos_de_estacionamento).collect(Collectors.toSet());
        this.tempos.forEach(x -> x.setViatura(this));
    }

    public Viatura(String matricula){
        this.matricula = matricula;
    }

    @Override
    public String toString() {
        return String.format("Viatura [matricula='%s']", matricula);
    }

    public String getMatricula() {
        return matricula;
    }
}
