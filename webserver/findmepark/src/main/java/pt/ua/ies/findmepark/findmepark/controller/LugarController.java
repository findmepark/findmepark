package pt.ua.ies.findmepark.findmepark.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pt.ua.ies.findmepark.findmepark.model.Lugar;
import pt.ua.ies.findmepark.findmepark.model.Parque;
import pt.ua.ies.findmepark.findmepark.repository.LugarRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/Lugar")
@CrossOrigin(origins = "*")
public class LugarController {

    @Autowired
    LugarRepository repository;


    @PutMapping("/")
    public String create(@RequestBody Lugar lugar){
        repository.save(lugar);
        return "Lugar criado";
    }

    @DeleteMapping("/")
    public String delete(@RequestBody Lugar lugar) {
        repository.delete(lugar);
        return "Lugar removido";
    }

    @GetMapping("/findall")
    public List<Lugar> findAll(){
        return repository.findAll();
    }

    @RequestMapping("/search/{numero}/{andar}")
    public List<Lugar> search_lugar(@PathVariable String numero, int andar){
        return repository.findByNumeroOrAndar(numero,andar);
    }
}
