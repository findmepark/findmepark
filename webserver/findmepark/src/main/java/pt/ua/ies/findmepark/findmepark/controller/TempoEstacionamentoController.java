package pt.ua.ies.findmepark.findmepark.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pt.ua.ies.findmepark.findmepark.repository.TempoEstacionamentoRepository;
import pt.ua.ies.findmepark.findmepark.model.TempoEstacionamento;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/TempoEstacionamento")
@CrossOrigin(origins = "*")
public class TempoEstacionamentoController {

    @Autowired
    TempoEstacionamentoRepository repository;


    @PutMapping("/")
    public String create(@RequestBody TempoEstacionamento tempoestacionamento) {
        repository.save(new TempoEstacionamento(tempoestacionamento.getData_inicio(), tempoestacionamento.getData_fim()));
        return "TempoEstacionamento criado";
    }

    @DeleteMapping("/")
    public String delete(@RequestBody TempoEstacionamento tempoestacionamento){
        repository.delete(tempoestacionamento);
        return "TempoEstacionamento removida";
    }

    @GetMapping("/findall")
    public List<TempoEstacionamento> findAll(){

        return repository.findAll();
    }

}
