package pt.ua.ies.findmepark.findmepark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ua.ies.findmepark.findmepark.model.Parque;
import pt.ua.ies.findmepark.findmepark.model.Sugestao;

import java.util.List;


@Repository
public interface SugestaoRepository extends JpaRepository<Sugestao, Long> {

    List<Sugestao> findByAssunto(String assunto);
    List<Sugestao> findAll();

}