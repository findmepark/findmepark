package pt.ua.ies.findmepark.findmepark.model;

import javax.persistence.*;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data

@Entity
@Table(name = "sugestao")
public class Sugestao implements Serializable {


    private static final long serialVersionUID = -3009157732242241606L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "assunto")
    private String assunto;

    @Column(name = "mensagem")
    private String mensagem;

    @OneToOne(mappedBy = "sugestao", cascade = CascadeType.ALL ,fetch = FetchType.LAZY)
    private Localizacao localizacao_sugestao;


    @OneToOne(fetch = FetchType.LAZY , cascade = CascadeType.ALL)
    @JoinColumn(name = "userID")
    @JsonIgnore
    private Utilizador sugestaoUtilizador;

    protected Sugestao() {
    }

    public Sugestao(String assunto, String mensagem) {
        this.assunto = assunto;
        this.mensagem = mensagem;
    }

}
