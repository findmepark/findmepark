package pt.ua.ies.findmepark.findmepark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ua.ies.findmepark.findmepark.model.Utilizador;
import java.util.List;


@Repository
public interface UtilizadorRepository extends JpaRepository<Utilizador, Long> {

    List<Utilizador> findByNome(String nome);
    List<Utilizador> findAll();
    Utilizador findByEmail(String email);
    List<Utilizador> findByTelefone(int telefone);
    Utilizador findById(long id);
}