package pt.ua.ies.findmepark.findmepark.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pt.ua.ies.findmepark.findmepark.model.Lugar;
import pt.ua.ies.findmepark.findmepark.model.SensorLugar;
import pt.ua.ies.findmepark.findmepark.repository.SensorLugarRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/SensorPresenca")
@CrossOrigin(origins = "*")
public class SensorLugarController {

    @Autowired
    SensorLugarRepository repository;

    @PutMapping("/")
    public String create(@RequestBody SensorLugar sensorLugar) {
        repository.save(sensorLugar);
        return "SensorPresenca criado";
    }

    @GetMapping("/findall")
    public List<SensorLugar> findAll(){
        return repository.findAll();
    }

    @DeleteMapping("/")
    public String delete(@RequestBody SensorLugar sensorLugar){
        repository.delete(sensorLugar);
        return "SensorPresenca removido";
    }



}
