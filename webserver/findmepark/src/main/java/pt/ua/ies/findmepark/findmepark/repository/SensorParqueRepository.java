package pt.ua.ies.findmepark.findmepark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ua.ies.findmepark.findmepark.model.SensorParque;

import java.util.List;

@Repository
public interface SensorParqueRepository extends JpaRepository<SensorParque, String> {

    SensorParque findById(long id);

}