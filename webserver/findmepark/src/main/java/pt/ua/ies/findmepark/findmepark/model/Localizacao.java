package pt.ua.ies.findmepark.findmepark.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode(exclude = "parque")

@Data
@Entity
@Table(name = "localizacao")
public class Localizacao implements Serializable {

    private static final long serialVersionUID = -3009157732242241606L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "cidade")
    private String cidade;

    @Column(name = "latitude")
    private float latitude;

    @Column(name = "longitude")
    private float longitude;

    @Column(name = "morada")
    private String morada;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parque")
    @JsonIgnore
    private Parque parque;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sugestao")
    @JsonIgnore
    private Sugestao sugestao;


    @OneToOne(mappedBy = "local_manager")
    private Manager manager;

    protected Localizacao() {
    }

    public Localizacao(String cidade,String morada, double latitude, double longitude) {
        this.cidade = cidade;
        this.morada = morada;
        this.latitude = (float)latitude;
        this.longitude = (float)longitude;
    }

    @Override
    public String toString() {
        return String.format("Localizaçao [cidade='%s', latitude='%f', longitude='%f']", cidade, latitude, longitude);
    }

}
