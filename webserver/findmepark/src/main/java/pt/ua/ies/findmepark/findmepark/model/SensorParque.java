package pt.ua.ies.findmepark.findmepark.model;

import javax.persistence.*;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.redis.core.RedisHash;

@Data

@Table(name = "SensorParque")
@Entity
public class SensorParque implements Serializable {


    private static final long serialVersionUID = -3009157732242241606L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;


    @Column(name = "entradas")
    private int entradas;

    @Column(name = "saidas")
    private int saidas;


    //Relationships
    @OneToOne(mappedBy = "sensor")
    @JsonIgnore
    private Parque parque;

    protected SensorParque() {
    }

    public SensorParque(int entradas, int saidas) {
        this.entradas = entradas;
        this.saidas = saidas;
    }




    public long getId() {
        return id;
    }

    public int Ocupacao(){
        return (entradas-saidas);
    }

}