package pt.ua.ies.findmepark.findmepark.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@EqualsAndHashCode(exclude = "parques", callSuper = false)
@Table(name = "manager")
@Entity
public class Manager extends Utilizador implements Serializable{

    // Relations
    @OneToMany(mappedBy = "park_manager", cascade = CascadeType.ALL)
    private Set<Parque> parques;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(unique = true)
    private Localizacao local_manager;


    protected Manager() {
    }

    public Manager(String nome, String email, int telefone, String foto, String password , Parque... parques) {
        this(nome,email,telefone,foto,password);
        //this.id = super.getId();

        this.parques = Stream.of(parques).collect(Collectors.toSet());
        this.parques.forEach(x -> x.setPark_manager(this));
    }

    public Manager(String nome, String email, int telefone, String foto, String password){
        super(nome, email, telefone, foto,password);
        //this.id = super.getId();
    }

    @Override
    public String toString() {
        return String.format("Manager[nome='%s', email='%s', telefone='%d', foto='%s' , tipo= '%s']", super.getNome(), super.getEmail(), super.getTelefone(), super.getFoto(), super.getPassword());
    }

    public Set<Parque> getParques() {
        return parques;
    }
}
