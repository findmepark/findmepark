package pt.ua.ies.findmepark.findmepark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pt.ua.ies.findmepark.findmepark.model.SensorLugar;

import java.util.List;

@Repository
public interface SensorLugarRepository extends JpaRepository<SensorLugar, String> {

    SensorLugar findById(long id);
    List<SensorLugar> findAll();

}
