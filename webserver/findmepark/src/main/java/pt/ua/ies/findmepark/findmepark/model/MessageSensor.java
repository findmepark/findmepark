package pt.ua.ies.findmepark.findmepark;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;


public class MessageSensor {

    private String name;
    private int occuppied;

    public MessageSensor(@JsonProperty("name") String name, @JsonProperty("data") int occuppied) {
        this.name = name;
        this.occuppied = occuppied;
    }

    public String getName() {
        return name;
    }

    public int getOccuppied() {
        return occuppied;
    }

    @Override
    public String toString() {
        return "CustomMessage{name='" + name + ", occupied='" + occuppied +'\'' +
                '}';
    }
}
