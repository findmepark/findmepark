package pt.ua.ies.findmepark.findmepark.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pt.ua.ies.findmepark.findmepark.repository.*;
import pt.ua.ies.findmepark.findmepark.model.Sugestao;
import pt.ua.ies.findmepark.findmepark.model.Utilizador;
import pt.ua.ies.findmepark.findmepark.model.Parque;
import pt.ua.ies.findmepark.findmepark.model.Localizacao;

@RestController
@RequestMapping("/teste")
@CrossOrigin(origins = "*")
public class testeController {

    @Autowired
    UtilizadorRepository user_repository;
    @Autowired
    ParqueRepository parqueRepository;
    @Autowired
    LocalizacaoRepository localizacaoRepository;
    @Autowired
    ManagerRepository managerRepository;
    @Autowired
    ViaturaRepository viaturaRepository;
    @Autowired
    LugarRepository lugarRepository;
    @Autowired
    SensorLugarRepository sensorLugarRepository;
    @Autowired
    SensorParqueRepository sensorParqueRepository;
    @Autowired
    SugestaoRepository sugestaoRepository;
    @Autowired
    TempoEstacionamentoRepository tempoEstacionamentoRepository;

    @GetMapping("/create")
    public String create() {

        reset();

        // Parques
        Parque p = new Parque("Parque na Rua de Espinho",true,0);
        Parque p2 = new Parque("Parque 3",true,0);
        Parque p3 = new Parque("Parque UA",true,0);
        Parque p4 = new Parque("Parque ISCA-UA",true,0);
        Parque p5 = new Parque("Parque Santa Tecla", true, 1);


        // Utilizadores
        Utilizador a = new Utilizador("Utilizador1", "utilizador1@gmail.com", 987654321, "foto1","tipo");
        Utilizador b = new Utilizador("Utilizador2", "utilizador2@gmail.com", 123456789, "foto2","tipo");


        // Sugestaões
        Sugestao s = new Sugestao("novo parque", "novo parque no centro de saude");



        //Localizacao
        Localizacao n = new Localizacao("Aveiro","Rua de Espinho" ,20, 50);
        Localizacao n2 = new Localizacao("Aveiro","Rua Dr Mario Sacramento" ,240, 60);
        Localizacao n3 = new Localizacao("Aveiro", "centro de saude" , 300 , 400);
        Localizacao n4 = new Localizacao("Aveiro","Rua De Santiago" ,40.631019, -8.655071);
        Localizacao n5 = new Localizacao("Aveiro", "Av da Universidade", 40.629127, -8.654742);
        Localizacao n6 = new Localizacao("Braga", "Av Santa Tecla", 41.549291, -8.410250);
        s.setLocalizacao_sugestao(n3);
        n3.setSugestao(s);
        s.setSugestaoUtilizador(a);
        a.setSugestao(s);

        n.setParque(p);
        n2.setParque(p2);
        n4.setParque(p4);
        n5.setParque(p3);
        n6.setParque(p5);

        p.setLocalizacaoParque(n);
        p2.setLocalizacaoParque(n2);
        p3.setLocalizacaoParque(n5);
        p4.setLocalizacaoParque(n4);
        p5.setLocalizacaoParque(n6);
        // user_repository.save(b);

        parqueRepository.save(p);
        parqueRepository.save(p2);
        parqueRepository.save(p3);
        parqueRepository.save(p4);
        parqueRepository.save(p5);

        // System.out.println("Sugestão: " + s);
        sugestaoRepository.save(s);
        user_repository.save(a);
        return "teste criado";
    }


    @GetMapping("/reset")
    public String reset() {
        localizacaoRepository.deleteAll();
        parqueRepository.deleteAll();
        sugestaoRepository.deleteAll();
        sensorParqueRepository.deleteAll();
        sensorLugarRepository.deleteAll();
        lugarRepository.deleteAll();
        viaturaRepository.deleteAll();
        user_repository.deleteAll();
        managerRepository.deleteAll();
        tempoEstacionamentoRepository.deleteAll();
        return "delete";
    }

    @GetMapping("/ping")
    public String ping() {
        return "ping";
    }

}
