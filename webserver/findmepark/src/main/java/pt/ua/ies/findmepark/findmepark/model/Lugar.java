package pt.ua.ies.findmepark.findmepark.model;

import javax.persistence.*;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(exclude="sensor")
@Entity
@Table(name = "Lugar")
public class Lugar implements Serializable {

    private static final long serialVersionUID = -3009157732242241606L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "numero")
    private String numero;

    @Column(name = "deficiente")
    private boolean deficiente;

    @Column(name = "andar")
    private int andar;

    // Relationships
    //@OneToOne(cascade = CascadeType.ALL)
    //@JoinColumn(unique = true)
    @OneToOne(mappedBy = "lugar", cascade = CascadeType.ALL ,fetch = FetchType.LAZY)
    private SensorLugar sensor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    @JsonIgnore
    private Parque parque;


    protected Lugar() {
    }

    // Lugar sem sensor usado para a criação dos sensores do parque
    public Lugar(String numero, boolean deficiente, int andar) {
        this.numero = numero;
        this.deficiente = deficiente;
        this.andar = andar;
    }

    // Lugar com sensor associado
    public Lugar(String numero, boolean deficiente, int andar, SensorLugar sensor) {
        this(numero,deficiente,andar);
        this.setSensor(sensor);
    }

    @Override
    public String toString() {
        return String.format("Lugar[numero='%s', deficiente='%b', andar='%d']", numero, deficiente, andar);
    }

    public String getNumero() {
        return numero;
    }

    public boolean isDeficiente() {
        return deficiente;
    }

    public boolean isOcupado(){
        return this.sensor.isOcupado();
    }


    public int getAndar() {
        return andar;
    }
}

