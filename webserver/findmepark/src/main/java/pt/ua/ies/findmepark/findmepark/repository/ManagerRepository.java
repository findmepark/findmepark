package pt.ua.ies.findmepark.findmepark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ua.ies.findmepark.findmepark.model.Manager;

import java.util.List;


@Repository
public interface ManagerRepository extends JpaRepository<Manager, Long> {

    List<Manager> findByNome(String nome);
    List<Manager> findAll();

    List<Manager> findByEmail(String email);
    List<Manager> findByTelefone(int telefone);

}