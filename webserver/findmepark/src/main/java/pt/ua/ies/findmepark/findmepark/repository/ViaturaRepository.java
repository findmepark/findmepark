package pt.ua.ies.findmepark.findmepark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ua.ies.findmepark.findmepark.model.Viatura;

import java.util.List;


@Repository
public interface ViaturaRepository extends JpaRepository<Viatura, Long> {

    List<Viatura> findByMatricula(String nome);
    List<Viatura> findAll();
    Viatura findById(long id);

}