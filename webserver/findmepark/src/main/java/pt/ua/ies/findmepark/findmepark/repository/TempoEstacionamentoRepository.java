package pt.ua.ies.findmepark.findmepark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ua.ies.findmepark.findmepark.model.Manager;
import pt.ua.ies.findmepark.findmepark.model.TempoEstacionamento;

import java.util.List;


@Repository
public interface TempoEstacionamentoRepository extends JpaRepository<TempoEstacionamento, Long> {

    List<TempoEstacionamento> findAll();


}