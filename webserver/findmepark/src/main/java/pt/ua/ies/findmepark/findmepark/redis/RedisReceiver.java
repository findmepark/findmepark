package pt.ua.ies.findmepark.findmepark.redis;

import java.util.*;
import java.util.concurrent.CountDownLatch;

import javax.persistence.criteria.CriteriaBuilder.In;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import pt.ua.ies.findmepark.findmepark.model.*;
import pt.ua.ies.findmepark.findmepark.repository.LugarRepository;
import pt.ua.ies.findmepark.findmepark.repository.ParqueRepository;
import pt.ua.ies.findmepark.findmepark.repository.SensorLugarRepository;
import pt.ua.ies.findmepark.findmepark.repository.SensorParqueRepository;

public class RedisReceiver {

  private static final Logger LOGGER = LoggerFactory.getLogger(RedisReceiver.class);

  private CountDownLatch latch;

  @Autowired
  SensorLugarRepository repository;
  @Autowired
  SensorParqueRepository p_repository;
  @Autowired
  ParqueRepository parqueRepository;
  @Autowired
  LugarRepository lugarRepository;

  HashMap<Integer, Long> sensor_id_map;
  HashMap<Integer, Long> p_sensor_id_map;

  final ArrayList<String> p_nomes = new ArrayList<String>() {
    {
      add("Glicinias");
      add("Centro de saude");
      add("UA");
      add("Bombeiros");
      add("Hospital");
      add("Praça");
      add("Continente");
      add("MacDonalds");
      add("Parque_1");
      add("Parque_2");
      add("Parque_3");
    }
  };

  final ArrayList<Localizacao> locais = new ArrayList<Localizacao>(){
    {
      add(new Localizacao("Aveiro","Rua de Espinho" ,40.6246274, -8.6503581));
      add(new Localizacao("Aveiro","Rua de Beira-Mar" ,50, 50));
      add(new Localizacao("Aveiro","Praça" ,20, 70));
      add(new Localizacao("Aveiro","Avenida" ,80, 50));
      add(new Localizacao("Aveiro","Av. Artur Ravara" , 40.6268879, -8.6491879));
      add(new Localizacao("Aveiro","123 centro" ,20, 50));
      add(new Localizacao("Aveiro","Rua Principal" ,20, 50));
      add(new Localizacao("Aveiro","Rua Principal2" ,40, 40));
      add(new Localizacao("Aveiro","Rua Principal3" ,40, 40));
      add(new Localizacao("Aveiro","Rua Principal4" ,40, 40));
      add(new Localizacao("Aveiro","Rua Principal5" ,40, 40));
    }
  };


  @Autowired
  public RedisReceiver(CountDownLatch latch) {
    this.latch = latch;
    this.sensor_id_map = new HashMap<>();
    this.p_sensor_id_map = new HashMap<>();
  }

  public void receiveMessage(String bytes_message) {
    //LOGGER.info("Received <" + bytes_message + ">");

    // latch.countDown();
    String message = "";

    message = bytes_message;

    JSONParser parser = new JSONParser();
    JSONObject data = null;

    try {
      data = (JSONObject) parser.parse(message);
    } catch (ParseException e) {
      e.printStackTrace();
    }

    // LOGGER.info("Received <" + data + ">");
    
    int s_id = Integer.parseInt((String) data.get("id"));


    if (data.get("type").equals("SENSOR_CREATE")) {
      System.out.println("Create");
      if (data.get("sensor_type").equals("parking_space")) {
        SensorLugar s = new SensorLugar(false);
        Lugar l = new Lugar(Integer.toString((int) (Math.random()) * 2) , Math.random() > 0.7 , (int) (Math.random() * 2));

        l.setSensor(s);
        s.setLugar(l);

        //lugarRepository.save(l);

        Parque p = new Parque(p_nomes.get(0) , true , (double) Math.round( Math.random() * 100) / 100);
        l.setParque(p);
        Set<Lugar> a = new HashSet<>();
        a.add(l);
        p.setLugares(a);

        Localizacao b = locais.get(0);
        b.setParque(p);
        p.setLocalizacaoParque(b);
        locais.remove(0);
        p_nomes.remove(0);

        parqueRepository.save(p);
        this.sensor_id_map.put(s_id, s.getId());
      }
      if (data.get("sensor_type").equals("camera")) {

        SensorParque s = new SensorParque(0,0);
        Parque p = new Parque(p_nomes.get(0) , true , 0);
        p_nomes.remove(0);
        s.setParque(p);
        p.setSensor(s);

        Localizacao b = locais.get(0);
        b.setParque(p);
        p.setLocalizacaoParque(b);
        locais.remove(0);


        parqueRepository.save(p);
        this.p_sensor_id_map.put(s_id, s.getId());
      }

    } else if (data.get("type").equals("SENSOR_DATA")) {
      if (!(this.sensor_id_map.containsKey(s_id) || this.p_sensor_id_map.containsKey(s_id))) return;

      if (this.sensor_id_map.containsKey(s_id)) {           // Sensor Lugar
        long repo_s_id = this.sensor_id_map.get(s_id);
        boolean s_ocupado = (long) data.get("data") == (long) 1;

        SensorLugar s = repository.findById(repo_s_id);
        s.setOcupado(s_ocupado);
        repository.save(s);
        }

      if (this.p_sensor_id_map.containsKey(s_id)){        // Sensor Parque
        data = (JSONObject) data.get("data");
        long repo_s_id = this.p_sensor_id_map.get(s_id);
        SensorParque sp = p_repository.findById(repo_s_id);
        if (data.get("type").equals("IN")){
          sp.setEntradas(sp.getEntradas() + 1);
        }
        if (data.get("type").equals("OUT")){
          // System.out.println("Someone got out!");
          sp.setSaidas(sp.getSaidas() + 1);
        }
        p_repository.save(sp);

      }

      }
    }
  }

