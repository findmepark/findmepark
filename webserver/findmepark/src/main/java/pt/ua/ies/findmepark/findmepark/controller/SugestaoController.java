package pt.ua.ies.findmepark.findmepark.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pt.ua.ies.findmepark.findmepark.model.Sugestao;
import pt.ua.ies.findmepark.findmepark.repository.SugestaoRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/Sugestao")
@CrossOrigin(origins = "*")
public class SugestaoController {

    @Autowired
    SugestaoRepository repository;


    @PutMapping("/")
    public String create(@RequestBody Sugestao sugestao){
        repository.save(sugestao);
        return "Sugestao criado";
    }

    @GetMapping("/findall")
    public List<Sugestao> findAll(){
        return repository.findAll();
    }

    @DeleteMapping("/")
    public String delete(@RequestBody Sugestao sugestao){
        repository.delete(sugestao);
        return "Sugestao removida";
    }

    @RequestMapping("/searchbyassunto/{assunto}")
    public List<Sugestao> fetchDataByNome(@PathVariable String assunto){
        return repository.findByAssunto(assunto);
    }


}
