package pt.ua.ies.findmepark.findmepark.model;

import javax.persistence.*;
import java.io.Serializable;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.Data;
import lombok.EqualsAndHashCode;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@EqualsAndHashCode(exclude = "lugares")
@Data
@Entity
@Table(name = "Parque")
@JsonAutoDetect(fieldVisibility = Visibility.ANY) 
public class Parque implements Serializable {


    private static final long serialVersionUID = -3009157732242241606L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "ocupacao")
    private int ocupacao;

    @Column(name = "disponivel")
    private boolean disponivel;

    @Column(name = "preco")
    private Double preco;

    //Relationship
    // Lugar - one to many
    @OneToMany(mappedBy = "parque",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Lugar> lugares;

    // Localizacao - one to one
    @OneToOne(mappedBy = "parque", cascade = CascadeType.ALL ,fetch = FetchType.LAZY)
    private Localizacao localizacaoParque;

    // SensorParque - one to one
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(unique = true)
    private SensorParque sensor;

    // Manager cada parque tem um manager, um manager tem varios parques
    @ManyToOne
    @JoinColumn
    private Manager park_manager;


    @OneToMany(mappedBy = "tempos_parque", cascade = CascadeType.ALL)
    private Set<TempoEstacionamento> tempoEstacionamentos;



    protected Parque() {
    }

    // Construtor para parques com sensores individuais
    public Parque(String nome_parque, boolean disponivel, double preco, Localizacao localizacaoParque, Lugar... lugares) {  // Lugar ... -> numero variavel de lugares como argumento, 0 ou mais
        this(nome_parque,disponivel,preco, localizacaoParque);
        // Lugares
        this.lugares = Stream.of(lugares).collect(Collectors.toSet());
        this.lugares.forEach(x -> x.setParque(this));
        // Ocupação dada a partir dos lugares que por sua vez obtêm a partir dos sensores
        this.ocupacao = getLugaresOcupados();

    }

    // Construtor para parques com um unico sensor à entrada
    public Parque(String nome_parque, boolean disponivel, double preco, Localizacao localizacaoParque, SensorParque sensor) {
        this(nome_parque,disponivel,preco, localizacaoParque);
        // Lugares
        this.sensor = sensor;
        // Ocupação
        this.ocupacao = getLugaresOcupados();
        // sensor.setParque(this);
    }

    // Construtor para parques com localização
    public Parque(String nome_parque, boolean disponivel,double preco, Localizacao localizacaoParque){
        this(nome_parque,disponivel,preco);
        this.localizacaoParque = localizacaoParque;
    }

    // Construtor base
    public Parque(String nome_parque, boolean disponivel, double preco){
        this.nome = nome_parque;
        this.disponivel = disponivel;
        this.ocupacao = getLugaresOcupados();
        this.preco = preco;
        this.id=getId();
    }



    @Override
    public String toString() {
        this.ocupacao = getLugaresOcupados();
        String s = String.format("Parque[id=%d , nome_parque='%s', ocupacao='%d', disponivel='%b'", id, nome, ocupacao, disponivel);
        if (this.lugares != null)
            s += "[lugares=" + this.lugares.toString() + "]";
        if (this.localizacaoParque != null)
            s += "[localizacao_parque=" + this.localizacaoParque.toString() + "]";
        return s + "]";
    }



    public int getLugaresOcupados() {
        this.ocupacao = 0;
        if (this.sensor!= null){        // Caso seja um unico sensor
            return sensor.Ocupacao();
        }
        if (this.lugares!=null){
            for (Lugar l:this.lugares){     // Caso seja com varios sensores
                if (l.isOcupado()){
                    this.ocupacao += 1;
                }
            }
        }
        return ocupacao;
    }

}
