package pt.ua.ies.findmepark.findmepark.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.Data;

@Data

@Entity
@Table(name = "utilizador")
@Inheritance(strategy=InheritanceType.JOINED)
public class Utilizador implements Serializable {

    private static final long serialVersionUID = -3009157732242241606L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "email")
    private String email;

    @Column(name = "telefone")
    private int telefone;

    @Column(name = "foto")
    private String foto;

    @Column(name = "password")
    private String password;

    @Column(name = "favoritos")
    private long[] parques_favoritos;


    //Relationship
    // Viaturas - one to many
    @OneToMany(mappedBy = "utilizador", cascade = CascadeType.ALL)
    private Set<Viatura> viaturas;


    @OneToOne(mappedBy = "sugestaoUtilizador", cascade = CascadeType.ALL)
    private Sugestao sugestao;

    protected Utilizador(){
    }

    public Utilizador(String nome, String email, int telefone, String foto ,String password, Viatura... viaturas) {
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
        this.foto = foto;
        this.password = password;
        this.viaturas = Stream.of(viaturas).collect(Collectors.toSet());
        this.viaturas.forEach(x -> x.setUtilizador(this));
    }

    public Utilizador(String nome, String email, int telefone, String foto , String password) {
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
        this.foto = foto;
        this.password = password;
    }

    public Utilizador(String nome, String email, String password, int telefone) {
        this.nome = nome;
        this.email = email;
        this.password = password;
        this.telefone = telefone;
    }
}
