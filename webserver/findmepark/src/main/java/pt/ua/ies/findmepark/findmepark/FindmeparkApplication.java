package pt.ua.ies.findmepark.findmepark;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.context.ApplicationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.ua.ies.findmepark.findmepark.repository.*;

@SpringBootApplication
public class FindmeparkApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(FindmeparkApplication.class);

	public static void main(String[] args) throws InterruptedException {


		ApplicationContext ctx = SpringApplication.run(FindmeparkApplication.class, args);

		StringRedisTemplate template = ctx.getBean(StringRedisTemplate.class);
		// CountDownLatch latch = ctx.getBean(CountDownLatch.class);

		// LOGGER.info("Sending message to chat...");
		// template.convertAndSend("chat", "Hello from Redis!");

		// latch.await();
	}

	
}
