package pt.ua.ies.findmepark.findmepark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ua.ies.findmepark.findmepark.model.Lugar;
import java.util.List;


@Repository
public interface LugarRepository extends JpaRepository<Lugar, Long> {

    List<Lugar> findByNumeroOrAndar(String numero, int andar);
    List<Lugar> findAll();

}