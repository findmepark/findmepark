package pt.ua.ies.findmepark.findmepark.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pt.ua.ies.findmepark.findmepark.model.Localizacao;
import pt.ua.ies.findmepark.findmepark.repository.LocalizacaoRepository;
import pt.ua.ies.findmepark.findmepark.repository.ParqueRepository;

import java.util.List;

@RestController
@RequestMapping("/Localizacao")
@CrossOrigin(origins = "*")
public class LocalizacaoController {

    @Autowired
    LocalizacaoRepository repository;

    @Autowired
    ParqueRepository p_repository;


    @PutMapping("/")
    public String create(@RequestBody Localizacao localizacao) {
        repository.save(localizacao);
        return "Localização criada";
    }

    @GetMapping("/findall")
    public List<Localizacao> findAll() {
        return repository.findAll();
    }

    @DeleteMapping("/")
    public String delete(@RequestBody Localizacao localizacao) {
        repository.delete(localizacao);
        return "Localização removida";
    }

    @GetMapping("/searchbycoordenadas/{latitude}/{longitude}")
    public List<Localizacao> findbyCoordinates(@PathVariable float latitude, @PathVariable float longitude) {
        return repository.findByLatitudeAndLongitude(latitude, longitude);
    }

    // Procura por cidade ou rua
    @GetMapping("/searchLocal/{local}")
    public List<Localizacao> findByCity(@PathVariable String local) {
        List<Localizacao> localizacoes = repository.findByCidadeContaining(local);
        localizacoes.addAll(repository.findByMoradaContaining(local));
        return localizacoes;
    }

    @GetMapping("/searchByCity/{cidade}")
    public List<Localizacao> find(@PathVariable String cidade) {
        return repository.findByCidadeLike(cidade);
    }



}
