package pt.ua.ies.findmepark.findmepark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ua.ies.findmepark.findmepark.model.Localizacao;

import java.util.List;

@Repository
public interface LocalizacaoRepository extends JpaRepository<Localizacao, Long> {

    List<Localizacao> findByCidadeLike(String nome);
    List<Localizacao> findByLatitudeAndLongitude(float latitude,float longitude);
    List<Localizacao> findAll();

    List<Localizacao> findByMoradaContaining(String nome);
    List<Localizacao> findByCidadeContaining(String nome);


}