package pt.ua.ies.findmepark.findmepark.controller;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pt.ua.ies.findmepark.findmepark.model.Manager;
import pt.ua.ies.findmepark.findmepark.repository.ManagerRepository;

import java.util.ArrayList;
import java.util.List;
@Data

@RestController
@RequestMapping("/Managers")
@CrossOrigin(origins = "*")
public class ManagerController {

    @Autowired
    ManagerRepository repository;


    @PutMapping("/")
    public String create(@RequestBody Manager manager) {
        repository.save(manager);
        return "Utilizador criado";
    }

    @GetMapping("/findall")
    public List<Manager> findAll() {
        List<Manager> Managers = repository.findAll();
        List<Manager> ManagerUI = new ArrayList<>();
        for (Manager manager : Managers) {
            ManagerUI.add(new Manager(manager.getNome(), manager.getEmail(), manager.getTelefone(), manager.getFoto() , manager.getPassword()));
        }
        return ManagerUI;
    }

    @DeleteMapping("/delete")
    public String delete(@RequestBody Manager manager) {
        repository.delete(manager);
        return "Manager removido";
    }

    @RequestMapping("/searchbynome/{nome}")
    public List<Manager> fetchDataByNome(@PathVariable String nome) {
        List<Manager> Managers = repository.findByNome(nome);
        List<Manager> ManagerUI = new ArrayList<>();
        for (Manager manager : Managers) {
            ManagerUI.add(new Manager(manager.getNome(), manager.getEmail(), manager.getTelefone(), manager.getFoto() , manager.getPassword()));
        }
        return ManagerUI;
    }

    @RequestMapping("/searchbytelefone/{telefone}")
    public List<Manager> fetchDataByTelefone(@PathVariable int telefone) {
        List<Manager> Managers = repository.findByTelefone(telefone);
        List<Manager> ManagerUI = new ArrayList<>();
        for (Manager manager : Managers) {
            ManagerUI.add(new Manager(manager.getNome(), manager.getEmail(), manager.getTelefone(), manager.getFoto() , manager.getPassword()));
        }
        return ManagerUI;

    }

    @RequestMapping("/searchbyemail/{email}")
    public List<Manager> fetchDataByEmail(@PathVariable String email) {
        List<Manager> Managers = repository.findByEmail(email);
        List<Manager> ManagerUI = new ArrayList<>();
        for (Manager manager : Managers) {
            ManagerUI.add(new Manager(manager.getNome(), manager.getEmail(), manager.getTelefone(), manager.getFoto() , manager.getPassword()));
        }
        return ManagerUI;
    }

}
