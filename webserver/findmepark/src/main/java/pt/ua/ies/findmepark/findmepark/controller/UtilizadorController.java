package pt.ua.ies.findmepark.findmepark.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import pt.ua.ies.findmepark.findmepark.model.Sugestao;
import pt.ua.ies.findmepark.findmepark.model.Utilizador;
import pt.ua.ies.findmepark.findmepark.repository.UtilizadorRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/Utilizadores")
@CrossOrigin(origins = "*")
public class UtilizadorController {

    @Autowired
    UtilizadorRepository repository;

    @PostMapping("/create")
    public ResponseEntity<Utilizador> create(@RequestBody Utilizador utilizador){
        repository.save(utilizador);
        return ResponseEntity.ok(utilizador); 
    }


    @PostMapping("/login")
    public ResponseEntity<Utilizador> login(@RequestBody Utilizador utilizador){
        Utilizador a = repository.findByEmail(utilizador.getEmail());
        if (utilizador.getPassword().equals(a.getPassword()))
            return ResponseEntity.ok(utilizador); 
        else
            return ResponseEntity.status(401).body(utilizador);
    }


    @DeleteMapping("/")
    public String delete(@RequestBody Utilizador utilizador){
        repository.delete(utilizador);
        return "Utilizador removido";
    }

    @GetMapping("/findall")
    public List<Utilizador> findAll(){
        return repository.findAll();

    }

    @RequestMapping("/searchbyid/{id}")
    public Utilizador searchID(@PathVariable long id){
        return repository.findById(id);
    }

    @RequestMapping("/searchbyemail/{email}")
    public Utilizador searchEmail(@PathVariable String email){
        return repository.findByEmail(email);
    }

    @RequestMapping("/searchbynome/{nome}")
    public List<Utilizador> searchNome(@PathVariable String nome){
        return repository.findByNome(nome);
    }

    @RequestMapping("/searchbytelefone/{telefone}")
    public List<Utilizador> searchTelefone(@PathVariable int telefone){
        return repository.findByTelefone(telefone);
    }

}
