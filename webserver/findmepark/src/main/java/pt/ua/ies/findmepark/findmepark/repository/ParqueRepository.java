package pt.ua.ies.findmepark.findmepark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ua.ies.findmepark.findmepark.model.Localizacao;
import pt.ua.ies.findmepark.findmepark.model.Parque;
import java.util.List;
import java.util.Optional;


@Repository
public interface ParqueRepository extends JpaRepository<Parque, Long> {

    List<Parque> findByNome(String nome);
    List<Parque> findByDisponivel(boolean disponivel);
    List<Parque> findAll();
    List<Parque> findByLocalizacaoParque_Id(long id);
    Parque findById(long id);

    List<Parque> findByLocalizacaoParque_Cidade(String cidade);
    List<Parque> findByLocalizacaoParque_Morada(String morada);

}
