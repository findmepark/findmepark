package pt.ua.ies.findmepark.findmepark.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pt.ua.ies.findmepark.findmepark.model.SensorParque;
import pt.ua.ies.findmepark.findmepark.repository.SensorParqueRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/SensorParque")
public class SensorParqueController {

    @Autowired
    SensorParqueRepository repository;

    @PutMapping("/")
    public String create(@RequestBody SensorParque sensorParque) {
        repository.save(sensorParque);
        return "SensorPresenca criado";
    }

    @GetMapping("/findall")
    public List<SensorParque> findAll(){
        return repository.findAll();
    }

    @DeleteMapping("/")
    public String delete(@RequestBody SensorParque sensorparque){
        repository.delete(sensorparque);
        return "SensorPresenca removido";
    }



}