package pt.ua.ies.findmepark.findmepark.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.*;
import java.io.Serializable;

import lombok.Data;

@Data
@EqualsAndHashCode(exclude="lugar")
@Table(name = "SensorLugar")
@Entity
public class SensorLugar implements Serializable {


    private static final long serialVersionUID = -3009157732242241606L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;


    @Column(name = "ocupado")
    private boolean ocupado;


    //Relationships
    //@OneToOne(mappedBy = "sensor")
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sensor")
    @JsonIgnore
    private Lugar lugar;

    protected SensorLugar() {
    }

    public SensorLugar(boolean ocupado) {
        this.ocupado = ocupado;
    }

    public SensorLugar(boolean ocupado , Lugar lugar) {
        this.ocupado = ocupado;
        this.lugar = lugar;
    }

    public long getId() {
        return id;
    }

    public boolean isOcupado() {
        return ocupado;
    }

}
