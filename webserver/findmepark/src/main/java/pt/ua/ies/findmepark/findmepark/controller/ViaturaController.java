package pt.ua.ies.findmepark.findmepark.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pt.ua.ies.findmepark.findmepark.model.Utilizador;
import pt.ua.ies.findmepark.findmepark.model.Viatura;
import pt.ua.ies.findmepark.findmepark.repository.ViaturaRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/Viaturas")
@CrossOrigin(origins = "*")
public class ViaturaController {

    @Autowired
    ViaturaRepository repository;


    @PutMapping("/")
    public String create(@RequestBody Viatura viatura) {
        repository.save(viatura);
        return "Viatura criada";
    }

    @DeleteMapping("/delete")
    public String delete(@RequestBody Viatura viatura){
        repository.delete(viatura);
        return "Viatura removido";
    }

    @GetMapping("/findall")
    public List<Viatura> findAll(){
        return repository.findAll();
    }

    @RequestMapping("/searchbyid/{id}")
    public Viatura search_id(@PathVariable long id){
        return repository.findById(id);
    }

    @RequestMapping("/searchbymatricula/{matricula}")
    public List<Viatura> search_matricula(@PathVariable String matricula){
        return repository.findByMatricula(matricula);
    }
}
